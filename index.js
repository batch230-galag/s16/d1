console.log("Hello World")

// Arithmetic Operators
    let x = 3;
    let y = 10;

    let sum = x + y;
    console.log("Result of addition operator: " + sum);
    
    let difference = x -y;
    console.log("Result of subraction operator: " + difference)

    let product = x * y;
    console.log("Result of multiplication operator: " + product);

    let quotient = x / y;
    console.log("Result of division operator: " + quotient);

    let remainder = y % x;
    console.log("Result of the modulo operator: " + remainder);

// Assignment operator ( = )
// Basic Assignment Operator (=)
    // The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

    let assignmentNumber = 8
    assignmentNumber = assignmentNumber + 2;
    console.log("Result of addition operator: " + assignmentNumber);

// Addition assignment operator
    assignmentNumber += 2;
    console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction assignment operator
    assignmentNumber -= 2;
    console.log("Result of subtraction assignment operator: " + assignmentNumber);

// Multiplication assignment operator
    assignmentNumber *= 2;
    console.log("Result of multiplication assignment operator: " + assignmentNumber);

// Divison assignment operator
    assignmentNumber /= 2;
    console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parenthesis

    let mdas = 1 + 2 - 3 * 4 / 5;
    console.log("Result of MDAS: " + mdas);

/*
        - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
        - The operations were done in the following order:
            1. 3 * 4 = 12
            2. 12 / 5 = 2.4
            3. 1 + 2 = 3
            4. 3 - 2.4 = 0.6
*/

// The order of operations can be changed by adding parenthesis
// PEMDAS
    let pemdas = 1 + (2 - 3) * (4 / 5);
    console.log("Result of PEMDAS: " + pemdas);

/*
    - By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule.
    - The operations were done in the following order:
        1. 2 - 3 = 0.8
        2. 4 / 5 = -1
        3. 1 + -1 = 0
        4. 0 * 0.8 = 0
*/

// Increment (++) and Decerement (--)
    let z = 1;

    // pre-increment
    let increment = ++z;
    console.log("Result of pre-increment: " + increment);
    console.log("Result of pre-increment (z): " + z);

    // post-increment
    increment = z++;
    console.log("Result of post-increment: " + increment);
    console.log("Result of post-increment (z): " + z);

    // increment = z++;
    // console.log(increment);

    // pre-decrement
    let decrement = --z;
    console.log("Result of pre-derecement: " + decrement);
    console.log("Result of pre-derecement (z): " + z);
    
    // post-decrement
    decrement = z--;
    console.log("Result of post-derecement: " + decrement);
    console.log("Result of post-derecement (z): " + z);

// Type coercion
/*
    - Type coercion is the automatic or implicit conversion of values from one data type to another
    - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
    - Values are automatically converted from one data type to another in order to resolve operations
*/
    // String and Number
    let numA = '10';
    let numB = 12;

    let coercion = numA + numB;
    console.log(coercion);
    console.log(typeof coercion);

    let numC = 16;
    let numD = 14;
    let nonCoercion = numC + numD;
    console.log(nonCoercion);
    console.log(typeof nonCoercion);

    // Boolean and Number
    let numE = true + 1;
    console.log(numE);
    console.log(typeof numE);

    let numF = false + 1;
    console.log(numF);
    console.log(typeof numF);

// Equality operator (==) "is Equal"
// we could read our equality operator with 'is equal
// disregards data types
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/
    console.log("--------------");

    let juan = 'juan';
    console.log(1 == 1);
    console.log(1 == 2);
    console.log(1 =='1');
    console.log('juan' == 'juan');
    console.log(juan == 'juan');

    console.log("--------------");
// Inequality operator (!=)
// NOT equal

    console.log(1 != 1);
    console.log(1 != 2);
    console.log(1 !='1');
    console.log('juan' != 'juan');
    console.log('juan' != juan);

// Strict Equality Operator (===)
/* 
    - Checks whether the operands are equal/have the same content
    - Also COMPARES the data types of 2 values
    - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
    - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
    - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
    - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/
    console.log("--------------");
    
    console.log(1 === 1);
    console.log(1 === 2);
    console.log(1 === '1');
    console.log(0 === false);
    console.log('juan' === 'juan');
    console.log(juan === 'juan');

// Strict Inequality Operator (!==)
/* 
    - Checks whether the operands are not equal/have the same content
    - Also COMPARES the data types of 2 values
*/
    console.log("--------------");

    console.log(1 !== 1);
    console.log(1 !== 2);
    console.log(1 !== '1');
    console.log(0 !== false);
    console.log('juan' !== 'juan');
    console.log(juan !== 'juan');

// Relational Operators
// Some comparison operators check whether one value is greater or less than to the other value.

    console.log("--------------");
    
    let a = 50;
    let b = 65;

    let isGreaterThan = a > b;
    let isLessThan = a < b;
    let isGTorEqual = a >= b;
    let isLTorEqual = a <= b;
    console.log(isGreaterThan);
    console.log(isLessThan);
    console.log(isGTorEqual);
    console.log(isLTorEqual);

// Logical Operators (&& - AND, || - OR, ! - NOT)

    console.log("--------------");

    let isLegalAge = true;
    let isRegistered = true;
    let isMarried = false;

    // Logical AND Operator ( && )
    // All requirements must be met
    let isLegalAgeAndisRegistered = isLegalAge && isRegistered;
    console.log("Result of logical AND operator: " + isLegalAgeAndisRegistered)

    let allCondition = isLegalAge && isRegistered && isMarried;
    console.log("Result of logical AND operator: " + allCondition)

    // Logical OR operator ( || )
    let someRequirements = isLegalAge || isMarried;
    console.log("Result of logical OR operator: " + someRequirements);

    // Logial NOT operator ( ! )
    let someRequirementsNotMet = !isRegistered;
    console.log("Result of logical NOT operator: " + someRequirementsNotMet);

    // !isLegalAge -> false
    // !isMarried -> True